require 'test_helper'

class SpeakerTest < ActiveSupport::TestCase
  def setup
    @speaker = Speaker.new(name: 'John Doe', title: 'CEO, Acme Corporation', bio: 'Zion of sex will essentially shape a brilliant believer.
                              Est fortis advena, cesaris. When one remembers stigma and dimension, one is able to forget fear.', pic: 'http://placehold.it/150x300')
  end

  test 'speaker should be valid' do
    assert @speaker.valid?
  end
end
