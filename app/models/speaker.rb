class Speaker < ActiveRecord::Base

    has_attached_file :pic, styles: {
        :original => '300x450#'
    }

    validates_attachment_content_type :pic, :content_type => /\Aimage\/.*\Z/

end
