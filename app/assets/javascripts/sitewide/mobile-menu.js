$(function() {
    var element = $('#top-nav');
    var grabber = $('#menu-grabber');

    function isOpen() {
        if(element.hasClass('open')) {
            return true;
        } else {
            return false;
        }
    }

    function toggleMenu() {
        if(isOpen()) {
            element.removeClass('open');
        } else {
            element.addClass('open');
        }
    }

    $(grabber).click(function(e) {
        toggleMenu();
    });

});