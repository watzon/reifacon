var t;

$(function() {
    var end = new Date('01/27/2016 8:00 AM');
    //end.setTime( end.getTime() - 420000  );
    
    var dayHolder = $('.countDays .holder');
    var hourHolder = $('.countHours .holder');
    var minuteHolder = $('.countMinutes .holder');
    var secondHolder = $('.countSeconds .holder');
    
    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour * 24;
    
    function stopCountdown() {
        dayHolder.html('0');
        hourHolder.html('0');
        minuteHolder.html('0');
        secondHolder.html('0');
    }
    
    function appendZero(integer) {
        if (integer < 10) {
            integer = '0' + integer;   
        }
        
        return integer;
    }

    (function countdown() {

        var now = new Date();
        var distance = end - now;
        if (distance < 0) {

            clearInterval(t);
            stopCountdown();

            return;
        }
        
        var days = Math.floor(distance / _day);
        var hours = Math.floor((distance % _day) / _hour);
        var minutes = Math.floor((distance % _hour) / _minute);
        var seconds = Math.floor((distance % _minute) / _second);

        dayHolder.html(appendZero(days));
        hourHolder.html(appendZero(hours));
        minuteHolder.html(appendZero(minutes));
        secondHolder.html(appendZero(seconds));


        t = setTimeout(countdown, 1000);

    })();

});