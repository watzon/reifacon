$(function() {
    //var speakerElement  = $('#speakerList'),
    //
        speakerList     = [
            {
                name: 'Scott Whaley',
                title: 'President, REIFA and National REIA',
                bio: '<p>Scott Whaley is the leader of the largest real estate investor association in the country, National REIA, and founder of the newly launched Real Estate Investor and Funding Association (REIFA). He speaks nationwide promoting them and the crowdfunding and Peer2Peer movements. He is a leading voice for these communities with a background in commercial real estate, lending, hedge funds, and performance coaching that gives him a unique perspective. He is passionate about creating new ways for real estate investors to connect with others and resources that make it easier and faster and safer to invest. He believes that because business is becoming more socially oriented and that middlemen create less engagement, that every individuals future success will be greatly affected by whether or not they find and join, a trusted and supportive community where they can connect face-to-face, peer2peer. Creating and expanding communities that support peer2peer engagement is how he sees his role in assisting NationalREIA and REIFA and each individual member become more successful.</p><p>Scott has over 15 years experience in the real estate industry and 8 years in the peak performance coaching industry as well. He’s developed over $30,000,000 of real estate and sold over $50,000,000 in commercial real estate, 3 years as a mortgage lender, and at the age of 26 founded Star Tex Enterprises Real Estate Investments & Brokerage. With his experience as a professional tennis player, trainer of Neuro Linguistic Programming, Master Coach for Anthony (Tony) Robbins, Speaker, Investor and Business Owner and focusing on two main projects; bringing National REIA to new levels of professionalism, size and influence AND launching Nationals new sister Association, REIFA (Real Estate Investment and Funding Association).</p><p> Scott has been a board member for the National Real Estate Association for over eight (8) years. During that time he has served two terms as Vice President and is currently serving as President.</p><p> In addition to his duties as President for National REIA, Scott just launched the Real Estate Investment & Funding Association (REIFA). REIFA is the first and only non-profit association for Non-Institutional Real Estate Investment & Funding alternatives, complete with education, deal matching, networking and a strong belief that giving and collaboration are the secret ingredients to outstanding results and performance in any area of life.</p>',
                pic: 'scott.jpg'
            },
            {
                name: 'Richard Swart',
                title: 'Partner & Director of Research, UC Berkeley',
                bio: '<p>A PhD in Information Systems, an international award-winning academic & a recognized global thought leader in the crowdfunding industry. Richard is a founding board member of the Crowdfunding Professional Assoc., the Crowdfunding Intermediary Regulatory Advocates, & an early leader in the field. Richard co-organized the first major national conference on crowdfunding & coordinated several educational events on the JOBS Act throughout the US for the White House.</p><p>As Director of Research for the University of California, Berkeley Program in Innovation in Entrepreneurial & Social Finance Program, which is dedicated to academic research on crowdfunding where he also organized the first International Academic Symposium on Crowdfunding Research. As Director of Research & Analysis for Crowdfund Capital Advisors he guides research that informs advisory services globally in crowdfunding policy, best practices & trends in emerging alternative finance markets.</p><p>Richard has emerged as the global expert in development sin crowdfunding. He was the lead author on a research project for the World Bank exploring crowdfunding & funding models for innovative technology. He has lectured in Europe, the UK, Africa, the Middle East & throughout the US. In recognition of his research in the UK, he was recently made a Fellow of the Royal Society of Arts.</p><p>Dr. Swart was the lead author or researcher on the World Bank’s report on Crowdfunding & it’s Potential Impact for the Developing World. He partnered with the University of Cambridge to complete the first Country level-study of alternative finance: The Rise of Future Finance: The UK Alternative Finance Benchmarking Report. With Crowdfund Capital Advisors he authored the report: How Does Crowdfunding Impact Job Creation, Revenue & Professional Investor Interest? –the first report that empirically demonstrated crowdfunding’s effects on job creation & future investments.</p>',
                pic: 'richard-stewart.jpg'
            },
            {
                name: 'Jillian Sidoti',
                title: 'Syndication Attorney, Law Office of Trowbridge, Taylor & Sidoti',
                bio: '<p>Jillian Sidoti of the Law Office of Trowbridge, Taylor & Sidoti is the expert on money raising techniques for real estate companies. After working in commercial real estate doing condo conversions for many years, Jillian started a firm that specializes in securities transactional legal matters such as Private Placement Memorandums, SEC compliance, Regulation A filings, Direct Public Offerings, and Intrastate Offerings. Specializing in transactional legal matters, Jillian Sidoti can assist clients who are looking to raise private money legally.</p><p>Jillian also spends her time speaking at seminars educating real estate investors on how to legally raise capital for their real estate investment projects.</p><p>Prior to her legal career, Jillian owned and operated a record label enabling her to tour worldwide with artists, including visits to South Africa, Canada, Europe, and the United States. Using that experience, Jillian has been commissioned to write articles and contracts for many music industry entities.</p><p>Currently, Jillian teaches Finance and Accounting for the BS and MBA programs at the University of Redlands, drawing on her experience as Financial Analyst, Controller, and CFO for many companies from manufacturing to real estate development. Jillian also teaches a Small Business Management class where students are taught the anatomy of a business plan.</p>',
                pic: 'jillian-sidoti.jpg'
            },
            {
                name: 'Eddie Speed',
                title: 'President, The Note School & Colonial Funding',
                bio: '<p>Since 1980, W. Eddie Speed has dedicated his professional life to the seller financing and non-performing note industry. Over the years, he has introduced innovative ideas and strategies that have positively impacted the way the industry operates today.</p><p>Eddie founded NoteSchool which is a highly recognized training company specialized in the teaching of buying both performing and non-performing discounted mortgage notes. He is the owner and president of Colonial Funding Group LLC, which acquires and brokers discounted real estate secured notes. In addition, he is also a principal in a family of Private Equity funds that acquires bulk portfolios of notes.</p><p>He has been a leader and innovator in the Note Business for over 30 years. He will tell you that those 30 plus years have prepared him for the incredible opportunities of this current real estate market.</p>',
                pic: 'eddie-speed.jpeg'
            },
            {
                name: 'Jan Brzeski',
                title: 'Managing Director & CIO, Arixa Capital Advisors',
                bio: '<p>Mr. Jan B. Brzeski is the Managing Director and Chief Investment Officer of Crosswind Financial and Arixa Capital Advisors, LLC, a private real estate investment advisor in Los Angeles that he founded in 2006. He is the fund manager of Arixa Fund I, Arixa Fund III, and Crosswind Venture Fund, which lend funds to investors who purchase, renovate, and resell single family homes. He also co-manages Arixa Fund II, which purchases, renovates and operates single family homes as rental properties. Mr. Brzeski has participated in more than 300 real estate transactions, ranging in value from $100,000 to over $20 million per transaction, including virtually every aspect of sourcing, structuring, financing, and acquiring investment properties. Mr. Brzeski’s insights into real estate trends and opportunities have been featured in the Los Angeles Times, Reuters, Bloomberg and the Los Angeles Business Journal. He is also the author of The Common Sense Guide to Real Estate Investing.</p><p>Prior to forming Arixa Capital, Mr. Brzeski was Vice President of Acquisitions at Standard Management Company, a highly regarded real estate investment and management company founded by Samuel K. Freshman. While at Standard Management, Mr. Brzeski’s responsibilities included identifying properties to acquire, underwriting, and negotiating to purchase income property and land. Mr. Brzeski has originated loans from funds he manages throughout Coastal California and has also purchased properties including shopping centers, industrial projects, agricultural and development land in Los Angeles, Kern, Riverside, San Bernardino, Placer, and Yolo Counties in California.</p><p>Before joining Standard Management, Mr. Brzeski was a successful entrepreneur. He co-founded and served as CEO of STV Communications, Inc., a media services businesses that he sold in 2000. Previously, Mr. Brzeski worked at Goldman, Sachs & Co. as an investment banking analyst, where he contributed to initial public offerings, secondary stock and bond offerings, as well as mergers and acquisitions of leading technology companies.</p><p>Mr. Brzeski has been the primary organizer of an annual Real Estate Investment Roundtable at UCLA Anderson School of Management for the past eight years. He is also an instructor for UCLA Extension’s Real Estate Program. Mr. Brzeski holds a B.A. in Physics from Dartmouth College and an M.A. in Economics (PPE) from Oxford University. He is a California licensed real estate broker.</p>',
                pic: 'jan-brzeski.jpeg'
            },
            {
                name: 'Jeff Watson',
                title: 'IRA Investor, Law Office of Jeff Watson',
                bio: '<p>Jeff has been a real estate investor since 1993. The Jeffery S. Watson Law Firm primarily deals with established real estate investors, particularly those who are using self-directed retirement accounts.</p><p>As a frequent and popular guest speaker he has become a recognized thought leader and innovator in the field of real estate investing.</p><p> From 2010 to present Jeff has led the Distressed Property Coalition; a Washington DC lobbying effort in bringing about several changes in government regulation of distressed property purchases and re-sales.</p><p> The new Standard National Short Sale Policy instituted by FHFA, effective Nov. 1st 2012 is due in part to the DPC and Jeff’s efforts. This means that Jeff and the DPC just changed the rules; for the better, on how investors and others buy and sell real estate.</p>',
                pic: 'jeff-watson.jpeg'
            },
            {
                name: 'Shari Peterson',
                title: 'Owner & Managing Member, Capri Capital LLC',
                bio: '<p>Shari began investing in real estate in 2007, focusing on short sale transactions and buying houses “subject-to” the existing mortgage. To this day, she still an active landlord. Real estate investing and providing Transactional Funding are the most recent business activities she has been involved in. Prior to this, she enjoyed a successful record of past business ventures.</p><p>The changing climate of real estate investing is what spurred the creation of Capri Capital, LLC. When flipping a property, title companies used to allow the funds from the B-C transaction to be used to pay-off the A-B transaction -- the investor did not have to bring any cash to the table. As the financial climate became much more conservative in the late 2000s, title companies tightened up their guidelines and began requiring that each deal be funded separately. This meant that the investor could no longer use the cash the C Buyer brought to the table to pass through and pay off the seller in the A-B transaction. As an investor herself, Shari was faced with needing to find private lenders to provide cash to allow her to close the A-B transaction. She soon realized that if she needed funds to close, so would many other real estate investors. That is how Capri Capital, LLC was born.</p> ',
                pic: 'shari-peterson.jpeg'
            },
            {
                name: 'Sydney Armani',
                title: 'CEO/Publisher, Crowdfund Beat',
                bio: '<p>Sydney Armani is a long time silicon valley entrepreneur, with more than twenty years experience in Valley\'s community acting in both an entrepreneurial and investment capacity. Sydney\'s vision for starting and successfully managing innovative companies like Hello Net Internet and Mobile telephony appliance services or Minitel and Videotex, a first generation of touch Screen tablets. His international experience in trade and International banking takes him around the world, projects with OPIC Overseas Private Investment Corporation for free trade with business engagement in Europe and UAE\'s Dubai.</p><p>A creative person at heart, he\'s working on building a crowdfunding platform "Live Crowdfunding demo pitch contest" building a bridge for new generation of Startup\'s in the Crowdfunding industry."</p><p>Sydney is CEO of Silicon Valley CrowdFund Ventures and the publisher of CrowdFundBeat, an online daily crowdfunding news site in US and UK, Canada, Germany, Italy, France, Holland. He is also the organizer of the annual Silicon Valley Meets Crowdfunders conference in Palo Alto, CA</p>',
                pic: 'sydney-armani.jpeg'
            }
        ];
    //
    //var template =  '<div class="imageset">' +
    //                    '<div class="photo-box" style="#{style}">' +
    //                        '<div class="overlay"></div>' +
    //                        '<div class="name-block">' +
    //                            '<div class="name">#{name}</div>' +
    //                            '<div class="title">#{title}</div>' +
    //                        '</div>' +
    //                        '<div class="modal">' +
    //                            '<a href="#" class="close-button">x</a>' +
    //                            '#{modalContent}' +
    //                        '</div>' +
    //                    '</div>' +
    //                '</div>';
    //
    //var numSpeakers = speakerList.length, i;
    //
    //
    //for(i=0;i<numSpeakers;i++) {
    //
    //    var modalContent =  '<div class="profile-pic">' +
    //                            '<img src="/images/speakers/' + speakerList[i].pic + '"/>' +
    //                        '</div>' +
    //                        '<h1 class="speaker-name">' + speakerList[i].name + '</h1>' +
    //                        '<h2 class="speaker-title">' + speakerList[i].title + '</h2>' +
    //                        '<div class="bio">' + speakerList[i].bio + '</div>';
    //
    //
    //    var modTemplate = template;
    //    modTemplate = modTemplate.replace('#{style}', 'background: url(/images/speakers/' + speakerList[i].pic + ');');
    //    modTemplate = modTemplate.replace('#{name}', speakerList[i].name);
    //    modTemplate = modTemplate.replace('#{title}', speakerList[i].title);
    //    modTemplate = modTemplate.replace('#{modalContent}', modalContent);
    //    speakerElement.append(modTemplate);
    //
    //}
    //
    //$('.imageset').click(function(e) {
    //    e.preventDefault();
    //    var modal = $(this).find('.modal');
    //    modal.fadeIn(300);
    //    $('#shades').fadeIn(300);
    //});
    //
    //$('body').on('click', '.modal .close-button', function(e) {
    //    e.preventDefault();
    //    $(this).parent().fadeOut(300);
    //    $('#shades').fadeOut(300);
    //});

    $('.imageset').click(function(e) {
        e.preventDefault();
        var modal = $(this).find('.modal');
        modal.fadeIn(300);
        $('#shades').fadeIn(300);
    });

    $('body').on('click', '.modal .close-button', function(e) {
        e.preventDefault();
        $(this).parent().fadeOut(300);
        $('#shades').fadeOut(300);
    });
    
});