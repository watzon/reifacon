$(function() {
    $('#new-speaker #name').keyup(function() {
        var text = $(this).val();
        text = text.toLocaleLowerCase();
        text = text.replace(/ /g, '_');

        $('#new-speaker #slug').val(text);
    });

    $("#new-speaker #slug").bind('change keyup input', function() {
        var slugCheck = $(this).val(), _this = $(this), submitButton = $('#new-speaker #submit');
        var json = $.getJSON('/admin/api/slug_check.json?slug=' + slugCheck, function (data) {

        })
            .done(function(data) {

                if (data.status === 409) {
                    _this.parent().addClass('has-error');
                    _this.addClass('form-control-error');
                    submitButton.prop('disabled', true);
                } else {
                    _this.parent().removeClass('has-error');
                    _this.removeClass('form-control-error');
                    submitButton.prop('disabled', false);
                }

                console.log(data.status);
            });
    });

});

