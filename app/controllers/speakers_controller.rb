class SpeakersController < ApplicationController
  def show
    @nav_with_bg = true
    @speaker = Speaker.where(slug: params[:slug]).first
  end
end
