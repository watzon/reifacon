class AdminController < ApplicationController

  layout 'admin'

    def index

    end

    def site_options

    end

    def show_speakers
        @speakers = Speaker.all
    end

    def new_speaker

    end

    def create_speaker
        @speaker = Speaker.create(speaker_params)

        if @speaker.valid?
            flash[:success] = 'Successfully created a new speaker'
            redirect_to(admin_show_speakers_path)
        else
            flash[:error] = @speaker.errors
            redirect_to(admin_new_speaker_path)
        end
    end

    def edit_speaker
        @speaker = Speaker.where(slug: params[:slug]).first
    end

    def update_speaker
        @speaker = Speaker.find(params[:speaker_id])
        if @speaker.update_attributes(speaker_params)
            redirect_to(admin_show_speakers_path)
        else
            redirect_to(admin_edit_speaker_path, slug: @speaker.slug)
        end
    end

    def delete_speaker
        Speaker.where(slug: params[:slug]).destroy_all
        redirect_to(admin_show_speakers_path)
    end

    def slug_check
        @speaker = Speaker.where(slug: params[:slug]).first
    end



    def speaker_params
        params.require(:speaker).permit(:name, :title, :slug, :bio, :pic, :facebook_url, :twitter_url, :linkedin_url)
    end
end
