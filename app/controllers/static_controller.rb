class StaticController < ApplicationController
  def index
      @speakers = Speaker.all
      @prices = [
          {
              price: '$199',
              date: '11/21/2015'
          },
          {
              price: '$299',
              date: '12/21/2015'
          },
          {
              price: '$399',
              date: '01/21/2016'
          },
          {
              price: '$499',
              date: '01/27/2016'
          }
      ]

  end
  def about

  end
  def agenda
    @nav_with_bg = true
  end
  def sponsors

  end
end
