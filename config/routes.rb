Rails.application.routes.draw do

  devise_for :users

  devise_scope :user do
      get '/admin/login' => 'devise/sessions#new'
      post '/admin/logout' => 'devise/sessions#create'
      delete '/admin/logout' => 'devise/sessions#destroy'
  end

  get 'speakers/show'

  get 'admin/index'

  root 'static#index'
  get '/about', to: 'static#about', as: 'about'
  get '/agenda', to: 'static#agenda', as: 'agenda'
  get '/sponsors', to: 'static#sponsors', as: 'sponsors'

  authenticate :user do
      get '/admin/', to: 'admin#index', as: 'admin_root'
      get '/admin/site_options', to: 'admin#site_options', as: 'admin_site_options'
      get '/admin/view/speakers', to: 'admin#show_speakers', as: 'admin_show_speakers'
      get '/admin/edit/speaker/:slug', to: 'admin#edit_speaker', as: 'admin_edit_speaker'
      get '/admin/add/speaker', to: 'admin#new_speaker', as: 'admin_new_speaker'
      get '/admin/api/slug_check', to: 'admin#slug_check', as: 'admin_slug_check'
      post '/admin/update/speaker', to: 'admin#update_speaker', as: 'admin_update_speaker'
      post '/admin/add/speaker', to: 'admin#create_speaker', as: 'admin_create_speaker'
      delete '/admin/delete/speaker/:slug', to: 'admin#delete_speaker', as: 'admin_delete_speaker'
  end


  get '/speaker/:slug', to: 'speakers#show', as: 'show_speaker'

end
