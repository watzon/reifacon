class AddSocialProfilesToSpeakers < ActiveRecord::Migration
  def change
    add_column :speakers, :facebook_url, :string
    add_column :speakers, :twitter_url, :string
    add_column :speakers, :linkedin_url, :string
  end
end
