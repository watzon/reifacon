class AddPicFileNameToSpeakers < ActiveRecord::Migration
  def change
      remove_column :speakers, :pic
      add_attachment :speakers, :pic
  end
end
