class CreateSpeakers < ActiveRecord::Migration
  def change
    create_table :speakers do |t|
      t.string :name
      t.string :title
      t.string :bio
      t.string :pic

      t.timestamps null: false
    end
  end
end
