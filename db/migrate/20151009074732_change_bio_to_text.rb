class ChangeBioToText < ActiveRecord::Migration
  def change
      remove_column :speakers, :bio
      add_column :speakers, :bio, :text
  end
end
